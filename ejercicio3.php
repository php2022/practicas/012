<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        $anchuraDados = 140;
        $alturaDados = 140;
        ?>
        <style type="text/css">
            img{
                width: <?= $anchuraDados ?>px;
                height: <?= $alturaDados ?>px;
            }
            .dados{
                width:<?= $anchuraDados * 2 ?>px;
            }

            .total{
                margin:10px;
            }

            .total>span{
                border: black 2px solid; 
                padding: 10px; 
                font-size: 300%;
            }
            .mayor{
                border: black 2px solid; 
                padding: 10px; 
                font-size: 100%;
                width:300px;
            }
        </style>
    </head>
    <body>
        <?php
        $contador = 0;
        $sumaTiradas = [];
        $tiradasEnumerado=[];
        $tiradasAsociativo=[];

        for ($c = 0; $c < 10; $c++) {
            $d1 = rand(1, 6);
            $d2 = rand(1, 6);
            $tiradasEnumerado[$c][0]=$d1;
            $tiradasEnumerado[$c][1]=$d2;
            $tiradasAsociativo[$c]["dado1"]=$d1;
            $tiradasAsociativo[$c]["dado2"]=$d2;
            $sumaTiradas[] = $d1 + $d2;
            ?>
            <div>
                <div class="dados">
                    <img src="imgs/<?= $d1 ?>.svg" alt="dado1"/>
                    <img src="imgs/<?= $d2 ?>.svg" alt="dado2"/>
                </div>
                <div class="total">Total: <span> <?= $sumaTiradas[$contador++] ?></span></div>
            </div>

            <?php
        }

        var_dump($sumaTiradas);
        var_dump($tiradasEnumerado);
        var_dump($tiradasAsociativo);
        ?>
        <div class="mayor">
            La tirada mayor ha sido de: <?= max($sumaTiradas) ?>
        </div>
    </body>
</html>
