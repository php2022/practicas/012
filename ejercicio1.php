<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            #suma{
                border: 2px solid black;
                width: 30px;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <?php
        $ancho="200px";
        $alto="200px";
                
        $dados=[1,2,3,4,5,6];
        
        $cuentaDados=count($dados);
        $indice=mt_rand(0,$cuentaDados-1);
        $dado=$dados[$indice];
        
        $cuentaDados1=count($dados);
        $indice1=mt_rand(0,$cuentaDados1-1);
        $dado1=$dados[$indice1];
        
        
        $suma=$dado+$dado1;
        
        
        
        ?>
        <div style="width: <?= ((int)$ancho)*2 ?>px">
            <img style="width: <?= $ancho ?> height:<?= $alto ?>" src="imgs/<?= $dado ?>.svg">
            <img style="width: <?= $ancho ?> height:<?= $alto ?>" src="imgs/<?= $dado1 ?>.svg">
        </div>
        <div>
            Total: 
            <div id="suma">
                <?= $suma ?>
            </div>
        </div>
    </body>
</html>
