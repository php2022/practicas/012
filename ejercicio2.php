<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .salida{
                border:1px solid black;
                width:80px;
                height:40px;
                line-height: 40px;
                text-align: center;
                display:inline-block;
            }
            .max{
                border: 1px solid black;
                width: 200px;
                padding: 5px;
                margin: 5px;
            }
        </style>
    </head>
    <body>
        <?php
        $dado1= [];
        $dado2= [];
        $suma=[];
        for($c=0;$c<10;$c++){
            $dado1[$c]= mt_rand(1,6);
            $dado2[$c]= mt_rand(1,6);
            $suma[$c]=$dado1[$c]+$dado2[$c];
        
        //var_dump($dado1,$dado2,$suma);
        
        ?>
        <div>
        <img src="imgs/<?= $dado1[$c] ?>.svg"/>
        <img src="imgs/<?= $dado2[$c] ?>.svg"/>
        </div>
        <div>
            Total <div class="salida"><?= $suma[$c] ?></div>
        </div>
        <?php
        }
        ?>
        
        <div class="max">La tirada mayor es: <?= max($suma) ?></div> 
    </body>
</html>
